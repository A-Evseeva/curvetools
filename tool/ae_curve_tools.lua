-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "AE_CurveTools"

-- **************************************************
-- General information about this script
-- **************************************************

AE_CurveTools = {}

function AE_CurveTools:Name()
	return self:Localize('UILabel')
end

function AE_CurveTools:Version()
	return '1.4'
end

function AE_CurveTools:UILabel()
	return self:Localize('UILabel')
end

function AE_CurveTools:Creator()
	return 'A.Evseeva'
end

function AE_CurveTools:Description()
	return self:Localize('Description')
end

function AE_CurveTools:LoadPrefs(prefs)
	self.selectMode = prefs:GetInt('AE_CurveTools.selectMode', self.SELECT_VISUALSEG)
	self.conformMode = prefs:GetInt('AE_CurveTools.conformMode', self.CONFORM_LENGTH)
end

function AE_CurveTools:SavePrefs(prefs)
	prefs:SetInt('AE_CurveTools.selectMode', self.selectMode)
	prefs:SetInt('AE_CurveTools.conformMode', self.conformMode)	
end

function AE_CurveTools:ResetPrefs()
	self.selectMode = self.SELECT_VISUALSEG
	self.conformMode = self.CONFORM_LENGTH
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function AE_CurveTools:IsRelevant(moho)
	return true
end

function AE_CurveTools:IsEnabled(moho)
	return true
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function AE_CurveTools:OnMouseDown(moho, mouseEvent)
	if self.targetSelectMode then return end
	if mouseEvent.ctrlKey then
		self.sketchPts = {}
		self.sketchPtsTiming = {}
		self.previousX = mouseEvent.startPt.x
		self.previousY = mouseEvent.startPt.y
		self.dragging = true
	end
end

function AE_CurveTools:OnMouseMoved(moho, mouseEvent)
	if self.targetSelectMode then return end
	if self.dragging then	
		local testX = mouseEvent.pt.x - self.previousX
		local testY = mouseEvent.pt.y - self.previousY
		local d = (testX * testX) + (testY * testY)
		if (d > LM_Freehand.pixelTolerance * LM_Freehand.pixelTolerance) then	
			local v = LM.Vector2:new_local()
			v:Set(mouseEvent.vec)
			table.insert(self.sketchPts, v)
			table.insert(self.sketchPtsTiming, os.clock())
			self.previousX = mouseEvent.pt.x
			self.previousY = mouseEvent.pt.y
		end
	end
	if not mouseEvent.ctrlKey then
		if not self.lassoMode then
			self.lassoMode = true
			LM_SelectPoints:OnMouseDown(moho, mouseEvent)
			return
		else
			LM_SelectPoints:OnMouseMoved(moho, mouseEvent)
		end
	end
	mouseEvent.view:DrawMe()	
end

function AE_CurveTools:OnMouseUp(moho, mouseEvent)

	local mesh = moho:Mesh()
	if not mesh then return end


	if self.targetSelectMode then 
		local followLayer, curveID, segID = mouseEvent.view:PickGlobalEdge(mouseEvent.pt)
		if not followLayer or curveID < 0 then return end
		
		local meshLayer = moho:LayerAsVector(followLayer)
		local targetMesh = meshLayer:Mesh()
		local curve = targetMesh:Curve(curveID)
		self.targetLayer = meshLayer
		self.targetLayerCurve = curve
		mouseEvent.view:DrawMe()
		return

	end
	
	if self.lassoMode then
		self.lassoMode = false
		LM_SelectPoints:OnMouseUp(moho, mouseEvent)
		self.lastClickedPoint = nil
		local dist = 1000
		for p=0, mesh:CountPoints()-1 do
			if mesh:Point(p).fSelected then
				local nextDist = (mouseEvent.vec - mesh:Point(p).fPos):Mag()
				if nextDist < dist then 
					dist = nextDist
					self.lastClickedPoint = mesh:Point(p)
				end
			end
		end
		mouseEvent.view:DrawMe()
		return
	end
	
	
	self.dragging = false
	if mouseEvent.ctrlKey then 		
		self:CreateFreehandCurve(moho)
		return
	end

	if not self.lastClickPt then 
		self.lastClickPt:Set(mouseEvent.pt)
		self.clickCounter = 0
	else
		local dif = self.lastClickPt - mouseEvent.pt
		if dif.x*dif.x + dif.y*dif.y < 0.00001 then
			self.clickCounter = self.clickCounter + 1
		else 
			self.lastClickPt:Set(mouseEvent.pt)
			self.clickCounter = 0
		end
	end


	
	
	
	if not mouseEvent.altKey then
		local curveID, segID = mouseEvent.view:PickEdge(mouseEvent.pt, curveID, segID)
		if curveID >= 0 then
			local p1 = LM.Vector2:new_local()
			local p2 = LM.Vector2:new_local()
			local p3 = LM.Vector2:new_local()
			local p4 = LM.Vector2:new_local()
			mesh:Curve(curveID):GetControlPoints(segID, p1, p2, p3, p4, true)
			local point = mesh:Curve(curveID):Point(segID)
			if (mouseEvent.vec - p1):Mag() > (mouseEvent.vec - p2):Mag() then
				local curvePoint = segID + 1
				if curvePoint >= mesh:Curve(curveID):CountPoints() then curvePoint = 0 end
				point = mesh:Curve(curveID):Point(curvePoint)
			end
			self.lastClickedPoint = point
		end	
	end
	
	local allwaysSelected = {}
	if mouseEvent.shiftKey or mouseEvent.altKey then
		for i=0, mesh:CountPoints()-1 do 
			if mesh:Point(i).fSelected then table.insert(allwaysSelected, mesh:Point(i)) end
		end
		mesh:SelectNone()
	end
	

	if self.selectModeMenu:FirstCheckedMsg() == self.SELECT_POINTGROUP then
		self:SelectPointGroup(moho, mouseEvent)
	else	
		self:SelectMulticurvePoints(moho, mouseEvent)
	end

	
	
	if mouseEvent.shiftKey then
		for i,v in pairs(allwaysSelected) do v.fSelected = true end
	elseif mouseEvent.altKey then
		local toSelect = {}
		for i,v in pairs(allwaysSelected) do 
			if not v.fSelected then table.insert(toSelect, v) end
		end
		mesh:SelectNone()
		for i,v in pairs(toSelect) do v.fSelected = true end
	end
	
end

function AE_CurveTools:OnKeyDown(moho, keyEvent)
	--print(tostring(keyEvent.ctrlKey), " ", keyEvent.keyCode)
	if keyEvent.keyCode == LM.GUI.KEY_BIND and self.targetSelectMode then
		if self.segmentArray and moho.document:IsLayerValid(self.targetLayer) and
		self.targetLayer:Mesh():CurveID(self.targetLayerCurve) >= 0 then
			self:CreateSelectedCopyCurve(moho)
		end
		self.targetSelectMode = false
		self.targetSelectCheck:SetValue(false)
	end	
end

function AE_CurveTools:OnKeyUp(moho, keyEvent)
	--print(tostring(keyEvent.ctrlKey), " ", keyEvent.keyCode)
end

function AE_CurveTools:DrawMe(moho, view)

	if self.lassoMode then
		LM_SelectPoints:DrawMe(moho, view)
	end

	local g = view:Graphics()
	local layerMatrix = LM.Matrix:new_local()	

	moho.layer:GetFullTransform(moho.frame, layerMatrix, moho.document)
	g:Push()
	g:ApplyMatrix(layerMatrix)
	g:SetSmoothing(true)
	g:SetBezierTolerance(2)	

	g:SetColor(MOHO.MohoGlobals.SelCol)
		
	if moho:CountSelectedPoints() <= 0 then self.segmentArray = {} end
	
	
	if self.segmentArray and #self.segmentArray > 0 then
		local seg = self.segmentArray[1]
		local pos = seg.curve:Point(seg.prePointID).fPos
		g:DrawDiamondMarker(pos.x, pos.y, 8)
		
		g:SetPenWidth(2)
		local steps = 6
		local r1, r2, curRange, curPos, lastPos, rangeStart
		for i,v in pairs(self.segmentArray) do
			rangeStart = v.dir and v.prePointID or v.postPointID
			r1, r2 = v.curve:GetSegmentRange(rangeStart)
			lastPos = nil
			for j=0, steps do
				curRange = r1 + (r2 - r1)*j/steps
				curPos = v.curve:GetPercentLocation(curRange)
				if lastPos then
					g:DrawLine(lastPos.x, lastPos.y, curPos.x, curPos.y)
				end
				lastPos = curPos
			end
		end
		
	end
	
	if self.dragging then
		g:SetPenWidth(1)
		for i = 1, #self.sketchPts - 1 do
			g:DrawLine(self.sketchPts[i].x, self.sketchPts[i].y, self.sketchPts[i + 1].x, self.sketchPts[i + 1].y)
		end	
		
	end
	

	
	

	if self.targetSelectMode and self.targetLayer and self.targetLayerCurve then
		if not moho.document:IsLayerValid(self.targetLayer) or 
		self.targetLayer:Mesh():CurveID(self.targetLayerCurve) < 0 then
			self.targetLayer = nil
			self.targetLayerCurve = nil
			self.targetSelectMode = false
			self.targetSelectCheck:SetValue(false)
			return 
		end
		self.targetLayer:GetFullTransform(moho.frame, layerMatrix, moho.document)

		g:Pop()
		g:Push()
		g:ApplyMatrix(layerMatrix)
		g:SetSmoothing(true)
		g:SetBezierTolerance(2)	

		g:SetColor(MOHO.MohoGlobals.ElemCol)
		g:SetPenWidth(3)
		local steps = 6		
		for i=0, self.targetLayerCurve:CountSegments()-1 do
			local r1, r2 = self.targetLayerCurve:GetSegmentRange(i)
			if r2 == 0 then r2 = 1.0 end
			lastPos = nil
			for j = 0, steps do
				curRange = r1 + (r2 - r1)*j/steps
				curPos = self.targetLayerCurve:GetPercentLocation(curRange)
				if lastPos then
					g:DrawLine(lastPos.x, lastPos.y, curPos.x, curPos.y)
				end
				lastPos = curPos				
			end
		end
	end

end

AE_CurveTools.curvature_accurance = 0.01
AE_CurveTools.circulefy_parameter = 0

AE_CurveTools.lastClickPt = LM.Point:new_local()
AE_CurveTools.clickCounter = 0
AE_CurveTools.targetSelectMode = false
AE_CurveTools.targetLayer = nil
AE_CurveTools.targetLayerCurve = nil

-- **************************************************
-- Tool Panel Layout
-- **************************************************

AE_CurveTools.FLIP = MOHO.MSG_BASE + 1
AE_CurveTools.FLIPPOINTS = MOHO.MSG_BASE + 2
AE_CurveTools.STRAIGHTEN = MOHO.MSG_BASE + 3
AE_CurveTools.CIRCULEFY = MOHO.MSG_BASE + 4
AE_CurveTools.SELECT_VISUALSEG = MOHO.MSG_BASE + 5
AE_CurveTools.SELECT_POINTGROUP = MOHO.MSG_BASE + 6
AE_CurveTools.CONFORM_LENGTH = MOHO.MSG_BASE + 7
AE_CurveTools.CONFORM_CORNERS = MOHO.MSG_BASE + 8
AE_CurveTools.TARGETSELECT_MODE = MOHO.MSG_BASE + 9
AE_CurveTools.FLIP_SELECTION_DIRECTION = MOHO.MSG_BASE + 10

AE_CurveTools.selectMode = AE_CurveTools.SELECT_VISUALSEG
AE_CurveTools.conformMode = AE_CurveTools.CONFORM_LENGTH

function AE_CurveTools:DoLayout(moho, layout)
	self.flipBtn = LM.GUI.Button(self:Localize('flip'), self.FLIP)
	layout:AddChild(self.flipBtn, LM.GUI.ALIGN_LEFT, 0)
	
	self.flipPointsBtn = LM.GUI.ImageButton("ScriptResources/flip_points_h", "Flip points around X", false, self.FLIPPOINTS, true)
	layout:AddChild(self.flipPointsBtn)
	
	layout:AddChild(LM.GUI.Divider(true))
	
	self.straightenBtn = LM.GUI.ImageButton("ScriptResources/curvetools/straighten_btn", "Straighten selected points", false, self.STRAIGHTEN, true)
	layout:AddChild(self.straightenBtn)
	
	self.circulefy_control = LM.GUI.TextControl(0, "00.000", self.CIRCULEFY, LM.GUI.FIELD_FLOAT)
	self.circulefy_control:SetWheelInc(0.1)
	layout:AddChild(self.circulefy_control)
	
	self.targetSelectCheck = LM.GUI.ImageButton("ScriptResources/curvetools/channel_line", "Select target curve to conform", true, self.TARGETSELECT_MODE, true)
	layout:AddChild(self.targetSelectCheck)
	
	self.selectModeMenu = LM.GUI.Menu("")
	self.selectModeMenu_popup = LM.GUI.PopupMenu(180, true)
	self.selectModeMenu_popup:SetMenu(self.selectModeMenu)
	self.selectModeMenu:AddItem("Select by visual segment", 0, self.SELECT_VISUALSEG)
	self.selectModeMenu:AddItem("Select by point group", 0, self.SELECT_POINTGROUP)
	layout:AddChild(self.selectModeMenu_popup)
	self.selectModeMenu_popup:SetToolTip("Select mode")
	self.selectModeMenu:SetChecked(self.SELECT_VISUALSEG, true)
	
	self.conformModeMenu = LM.GUI.Menu("")
	self.conformModeMenu_popup = LM.GUI.PopupMenu(150, true)
	self.conformModeMenu_popup:SetMenu(self.conformModeMenu)
	self.conformModeMenu:AddItem("Conform by length", 0, self.CONFORM_LENGTH)
	self.conformModeMenu:AddItem("Conform by corners", 0, self.CONFORM_CORNERS)
	layout:AddChild(self.conformModeMenu_popup)
	self.conformModeMenu_popup:SetToolTip("Conform mode")
	self.conformModeMenu:SetChecked(self.CONFORM_LENGTH, true)	
	
	self.flipSelDirectionButton = LM.GUI.ImageButton("ScriptResources/curvetools/channel_layer_flip_h", "Flip selection direction", false, self.FLIP_SELECTION_DIRECTION)
	layout:AddChild(self.flipSelDirectionButton)	
	
end

function AE_CurveTools:HandleMessage(moho, view, msg)

	if msg == self.FLIP then
		self:FlipCurve(moho)
	elseif msg == self.FLIPPOINTS then
		self:FlipPoints(moho)	
	elseif msg == self.STRAIGHTEN then
		self:StraightenSelectedPoints(moho)
	elseif msg == self.CIRCULEFY then
		if math.abs(self.circulefy_control:FloatValue()) > 1.00 then 
			self.circulefy_control:SetValue(self.circulefy_control:FloatValue()/math.abs(self.circulefy_control:FloatValue())) 
		end
		if self.circulefy_control:FloatValue() ~= self.circulefy_parameter then
			self.circulefy_parameter = self.circulefy_control:FloatValue()
			if(self.circulefy_parameter ~= 0) then self:CirculefySelectedPoints(moho) end
		end
	elseif msg == self.SELECT_VISUALSEG or msg == self.SELECT_POINTGROUP then
		self.selectMode = self.selectModeMenu:FirstCheckedMsg()
	elseif msg == self.CONFORM_LENGTH or msg == self.CONFORM_CORNERS then
		self.conformMode = self.conformModeMenu:FirstCheckedMsg()
	elseif msg == self.TARGETSELECT_MODE then
		self.targetSelectMode = self.targetSelectCheck:Value()
	elseif msg == self.FLIP_SELECTION_DIRECTION then
		self.segmentArray = self:InvertSegmentArray(moho, self.segmentArray)
	end
end

function AE_CurveTools:UpdateWidgets(moho)
	self.circulefy_parameter = 0
	self.selectModeMenu:UncheckAll()
	self.conformModeMenu:UncheckAll()
	self.selectModeMenu:SetChecked(self.selectMode, true)
	self.conformModeMenu:SetChecked(self.conformMode, true)
	if moho:Mesh() and moho:CountSelectedPoints() > 0 then 
		self:CollectVertexLineFromSelection(moho:Mesh()) 
	else 
		self.segmentArray = {}
	end
	self.targetSelectCheck:SetValue(self.targetSelectMode)
end

-- **************************************************
-- Work Methods
-- **************************************************

function AE_CurveTools:FlipPoints(moho)
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)
	
	local mesh = moho:Mesh()
	if not mesh then return LM.GUI.Alert(ALERT_WARNING, "Not a vector layer") end
	for p = 0, mesh:CountPoints()-1 do
		if mesh:Point(p).fSelected then 
			local point = mesh:Point(p).fAnimPos.value
			point.x = -point.x
			mesh:Point(p).fAnimPos:SetValue(moho.frame, point)
		end
	end
	
	for c=0, mesh:CountCurves()-1 do
		local curve = mesh:Curve(c)
		for p=0, curve:CountPoints()-1 do
			local point = curve:Point(p)
			if point.fSelected then
				local offset = curve:GetOffset(p, moho.frame + moho.layer:TotalTimingOffset(), true)
				curve:SetOffset(p, -offset, moho.frame + moho.layer:TotalTimingOffset(), true)
				offset = curve:GetOffset(p, moho.frame + moho.layer:TotalTimingOffset(), false)
				curve:SetOffset(p, -offset, moho.frame + moho.layer:TotalTimingOffset(), false)
			end
		end
	end							 
	moho.layer:UpdateCurFrame()
	moho:UpdateUI()
end

function AE_CurveTools:FlipCurve(moho)
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)
	
	local mesh = moho:Mesh()
	if not mesh then return LM.GUI.Alert(ALERT_WARNING, "Not a vector layer") end	
	-- get selected points 
	local selectedPoints = {}
	for p = 0, mesh:CountPoints()-1 do
		if mesh:Point(p).fSelected then table.insert(selectedPoints, p) end
	end
	
	-- conditions: 1 or 2 selected points and single point must belong to a single curve
	if not (#selectedPoints == 1 or #selectedPoints == 2) then return LM.GUI.Alert(ALERT_WARNING, "Select one or two center points") end
	if #selectedPoints == 1 and not (mesh:Point(selectedPoints[1]):CountCurves()==1) then
		return LM.GUI.Alert(ALERT_WARNING, "The single selected point must belong to a single curve")
	end
	-- get curve, if one point or if two points belongs only to one common curve
	local theCurve = nil
	if #selectedPoints == 1 then 
		theCurve = mesh:Point(selectedPoints[1]):Curve(0)
	else 
		local curves = {}
		for c = 0, mesh:Point(selectedPoints[1]):CountCurves()-1 do
			local curve = mesh:Point(selectedPoints[1]):Curve(c)
			if mesh:Point(selectedPoints[2]):IsPointOnCurve(mesh:CurveID(curve)) then
				table.insert(curves, mesh:CurveID(curve))
			end
		end
		if #curves == 0 then 
			return LM.GUI.Alert(ALERT_WARNING, "Select points on one curve")
		end
		if #curves > 1 then 
			return LM.GUI.Alert(ALERT_WARNING, "Too many curves between the same points")
		end
		theCurve = mesh:Curve(curves[1])
	end
	
	-- if first selected pair is not two neighbour points on curve, look for center pair or single between them
	firstPair = {theCurve:PointID(mesh:Point(selectedPoints[1]))}
	if #selectedPoints == 2 then table.insert(firstPair, theCurve:PointID(mesh:Point(selectedPoints[2]))) end
	table.sort(firstPair)
	if #firstPair == 2 then
		firstPairDistance = firstPair[2] - firstPair[1] 
		if firstPairDistance > 1 then
			for i=firstPairDistance, 1, -2 do 
				sm = firstPair[1] + 1
				bg = firstPair[2] - 1
				if sm == bg then firstPair = {sm} else firstPair = {sm,bg} end
			end
		end
	end
	
	-- collect other pairs moving from first point(s) in two directions
	local pointPairs = {firstPair}
	local numPairs = (theCurve:CountPoints()-#firstPair)/2
	if not theCurve.fClosed then
		if #firstPair == 1 then 
			numPairs = math.min(firstPair[1], theCurve:CountPoints()-1-firstPair[1]) 
		else 
			numPairs = math.min(firstPair[1], theCurve:CountPoints()-1-firstPair[2])
		end
	end
	while #pointPairs < (numPairs + 1) do
		local small = pointPairs[#pointPairs][1]
		local big = small
		if #pointPairs[#pointPairs]==2 then big = pointPairs[#pointPairs][2] end
		local newSmall = small - 1
		if newSmall < 0 then newSmall = theCurve:CountPoints()-1 end
		local newBig = big + 1 
		if newBig >= theCurve:CountPoints() then newBig = 0 end
		table.insert(pointPairs, {newSmall})
		if newBig ~= newSmall then table.insert(pointPairs[#pointPairs], newBig) end
	end
	
	-- collect all values for all curve's points
	local valuesCollection = {}
	for p=0, theCurve:CountPoints()-1 do
		valuesCollection[p] = {}
		valuesCollection[p].pos = LM.Vector2:new_local()
		valuesCollection[p].pos:Set(theCurve:Point(p).fAnimPos.value)
		valuesCollection[p].smooth = theCurve:GetCurvature(p, moho.frame)
		valuesCollection[p].inWeight = theCurve:GetWeight(p, moho.frame, true)
		valuesCollection[p].outWeight = theCurve:GetWeight(p, moho.frame, false)
		valuesCollection[p].inOffset = theCurve:GetOffset(p, moho.frame, true)
		valuesCollection[p].outOffset = theCurve:GetOffset(p, moho.frame, false)
		valuesCollection[p].lineWidth = theCurve:Point(p).fWidth.value
	end
	
	-- iterate pairs and set values using all point values collection
	for i, p in pairs(pointPairs) do
		local p1 = p[1]
		local p2 = p1
		if #p == 2 then p2 = p[2] end
		self:SetNewValues(moho, theCurve, p1, valuesCollection[p2])
		self:SetNewValues(moho, theCurve, p2, valuesCollection[p1])
	end	
	
	moho.layer:UpdateCurFrame()
	moho:UpdateUI()
	
	
end

-- set collected values to a point fliping IN and OUT handles
function AE_CurveTools:SetNewValues(moho, curve, pID, vals)
	curve:Point(pID).fAnimPos:SetValue(moho.frame, vals.pos)
	if curve:GetCurvature(pID, moho.frame)~= vals.smooth then curve:SetCurvature(pID, vals.smooth, moho.frame) end
	if curve:GetWeight(pID, moho.frame, true) ~= vals.outWeight then curve:SetWeight(pID, vals.outWeight, moho.frame, true) end
	if curve:GetWeight(pID, moho.frame, false) ~= vals.inWeight then curve:SetWeight(pID, vals.inWeight, moho.frame, false) end
	if curve:GetOffset(pID, moho.frame, true) ~= vals.outOffset then curve:SetOffset(pID, vals.outOffset, moho.frame, true) end
	if curve:GetOffset(pID, moho.frame, false) ~= vals.inOffset then curve:SetOffset(pID, vals.inOffset, moho.frame, false) end	
	if curve:Point(pID).fWidth.value ~= vals.lineWidth then curve:Point(pID).fWidth:SetValue(moho.frame, vals.lineWidth) end
end


function AE_CurveTools:FindSelectedNeighbour(pointObj, excludePointObj)
	for i=0, pointObj:CountCurves()-1 do
		local curve, curvePointID = pointObj:Curve(i)
		local prePointID = curvePointID - 1 
		if prePointID < 0 and curve.fClosed then prePointID = curve:CountPoints()-1 end
		if prePointID >= 0 and curve:Point(prePointID).fSelected and curve:Point(prePointID) ~= excludePointObj then
			return curve:Point(prePointID), curve, curvePointID, prePointID
		end
		local nextPointID = curvePointID + 1
		if nextPointID >= curve:CountPoints() and curve.fClosed then nextPointID = 0 end
		if nextPointID < curve:CountPoints() and curve:Point(nextPointID).fSelected and curve:Point(nextPointID) ~= excludePointObj then
			return curve:Point(nextPointID), curve, curvePointID, nextPointID
		end
	end
	return nil
end

--iterator getting first point and returning nextPoint, curve, point1, point2
function AE_CurveTools:IterateSelectedPoints(startPoint)
	local p1, p2 = startPoint, startPoint
	local cycleReached = false
	return function()
		if not p2 or not p1 or cycleReached then return nil end
		local nextPoint, curve, point1, point2 = self:FindSelectedNeighbour(p2, p1)
		p1, p2 = p2, nextPoint	
		if not p2 then return nil end
		if p2 == startPoint then cycleReached = true end
		return p1, p2, curve, point1, point2
	end	
end

function AE_CurveTools:FlipSegmentArray(sa)
	local newArray = {}
	for i=#sa, 1, -1 do
		table.insert(newArray, {["curve"] = sa[i].curve, ["prePointID"] = sa[i].postPointID, ["postPointID"] = sa[i].prePointID, ["dir"] = (not sa[i].dir)})
	end
	return newArray
end

function AE_CurveTools:CollectVertexLineFromSelection(mesh)
	
	local firstSelectedPoint = self.lastClickedPoint
	
	if not firstSelectedPoint or not firstSelectedPoint.fSelected or 
	mesh:PointID(firstSelectedPoint) < 0 then 
		for i=0, mesh:CountPoints()-1 do
			if mesh:Point(i).fSelected then
				firstSelectedPoint = mesh:Point(i)
				break
			end
		end		
	end
	
	if not firstSelectedPoint then 
			self.segmentArray = {}
			return nil
	end

	local fromClick2Start = 0
	local endPoint = nil
	for prevPoint, nextPoint in self:IterateSelectedPoints(firstSelectedPoint) do
		fromClick2Start = fromClick2Start + 1
		endPoint = nextPoint
	end
	if endPoint == firstSelectedPoint then
		fromClick2Start = 0
	end
	
	local segmentArray = {}
	
	for p1, p2, curve, point1, point2 in self:IterateSelectedPoints(endPoint) do	
		local dir = ((point2 > point1) and (point1 ~= 0 or point2 == 1)) or ( point2 ==0 and point1 ~= 1)
		table.insert(segmentArray, {["curve"] = curve, ["prePointID"] = point1, ["postPointID"] = point2, ["dir"] = dir})	
	end
		

	if firstSelectedPoint == endPoint then table.remove(segmentArray, #segmentArray) end
	
	if #segmentArray < fromClick2Start *2 then 
		segmentArray = self:FlipSegmentArray(segmentArray)
	end
	
	self.segmentArray = segmentArray
	return segmentArray
end

function AE_CurveTools:StraightenSelectedPoints(moho)
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)

	-- collect currently selected points to array of segments following by each other
	local mesh = moho:Mesh()
	if not mesh then return LM.GUI.Alert(LM.GUI.ALERT_WARNING, "Not found mesh") end
	--local segmentArray = self:CollectVertexLineFromSelection(mesh)
	if not self.segmentArray or #self.segmentArray < 1 then return end
	local segmentArray = self.segmentArray
	
	-- calculate total length of segment array pseudo-curve, and store each segment's current length to the same array members
	local totalLength = 0
	for i,v in pairs(segmentArray) do
		local segNo = math.min(v.prePointID, v.postPointID)
		if segNo == 0 and (v.prePointID ~= 1 and v.postPointID ~=1) then segNo = v.curve:CountPoints()-1 end
		v.length = v.curve:SegmentLength(segNo)
		totalLength = totalLength + v.length
	end
	
	-- iterate segment array calculating and setting each point's new position
	local startPoint = segmentArray[1].curve:Point(segmentArray[1].prePointID)
	local endPoint = segmentArray[#segmentArray].curve:Point(segmentArray[#segmentArray].postPointID)
	local curLength = 0
	for i,v in pairs(segmentArray) do
		if i < #segmentArray then
			curLength = curLength + v.length
			local pointLength = curLength/totalLength
			local newVec = startPoint.fPos + (endPoint.fPos - startPoint.fPos) * pointLength
			local point = v.curve:Point(v.postPointID)
			local delta = point.fPos - point.fAnimPos:GetValue(moho.frame)
			point.fAnimPos:SetValue(moho.frame, newVec - delta)
		end
	end
	moho.layer:UpdateCurFrame()
	for i,v in pairs(segmentArray) do
		--v.curve:SetCurvature(v.prePointID, 0.3, moho.frame)
		--v.curve:SetCurvature(v.postPointID, 0.3, moho.frame)

		local startCurvature = v.curve:GetCurvature(v.prePointID, moho.frame)
		local endCurvature = v.curve:GetCurvature(v.postPointID, moho.frame)

		local curHandle = v.curve:GetControlHandle(v.prePointID, moho.frame, not v.dir)
		local curHandleDir = curHandle - v.curve:Point(v.prePointID).fPos
		local curVector = endPoint.fPos - startPoint.fPos
		local angleDist = math.atan2(curVector.x, curVector.y) - math.atan2(curHandleDir.x, curHandleDir.y)
		local curOffset = v.curve:GetOffset(v.prePointID, moho.frame, not v.dir)
		v.curve:SetOffset(v.prePointID, curOffset - angleDist, moho.frame, not v.dir)

		curHandle = v.curve:GetControlHandle(v.postPointID, moho.frame, v.dir)
		curHandleDir = curHandle - v.curve:Point(v.postPointID).fPos
		curVector = startPoint.fPos - endPoint.fPos
		angleDist = math.atan2(curVector.x, curVector.y) - math.atan2(curHandleDir.x, curHandleDir.y)
		curOffset = v.curve:GetOffset(v.postPointID, moho.frame, v.dir)
		v.curve:SetOffset(v.postPointID, curOffset - angleDist, moho.frame, v.dir)

		if i < #segmentArray then v.curve:SetWeight(v.prePointID, startCurvature/0.3, moho.frame, not v.dir, false) end
		if i > 1 then v.curve:SetWeight(v.postPointID, endCurvature/0.3, moho.frame, v.dir, false) end		

	end
	
	moho.layer:UpdateCurFrame()
	moho:UpdateUI()
	
	self.circulefy_control:SetValue("0.000")
	self.circulefy_parameter = 0
end 

function AE_CurveTools:IsEndPoint(moho, curve, curvePointID)
	local offsetAccuracy = 0.1
	if curve:GetCurvature(curvePointID, moho.frame) == 0 then return true end
	if math.abs(curve:GetOffset(curvePointID, moho.frame, false)) > offsetAccuracy then return true end
	if math.abs(curve:GetOffset(curvePointID, moho.frame, true)) > offsetAccuracy then return true end
	
	return false
end

function AE_CurveTools:IterateCurvePoints(curve, startPoint, direction) 
	if not direction then direction = 1 end
	local counter = startPoint
	local iterateFn = function()
		counter = counter + direction	
		if counter >= curve:CountPoints() then
			if curve.fClosed then counter = 0 
			else return nil
			end
		end
		if counter < 0 then
			if curve.fClosed then counter = curve:CountPoints()-1 
			else return nil
			end
		end	
		if counter == startPoint then return nil end
		return counter
	end
	return iterateFn
end

function AE_CurveTools:PointOnCurveID(curve, point, closestPoint)
	for i = 0, curve:CountPoints()-1 do
		if curve:Point(i) == point then
			if not closestPoint then return i end
			if i > 0 and curve:Point(i-1) == closestPoint then return i end
			if i < curve:CountPoints()-1 and curve:Point(i+1) == closestPoint then return i end
			if i == curve:CountPoints()-1 and curve.fClosed and curve:Point(0) == closestPoint then return i end
		end
	end
	return -1
end

function AE_CurveTools:GetCommonSegment(moho, point1, point2)
	for c=0, point1:CountCurves()-1 do
		local where = -1
		local curve, where = point1:Curve(c, where)
		local where2 = self:PointOnCurveID(curve, point2, point1)
		if where2 >= 0 then
			local dif = math.abs(where-where2)
			if dif == 1 or (curve.fClosed and dif == (curve:CountPoints()-1)) then
				return curve, where, (not ((where2 > where and dif == 1) or (where2 == 0 and dif > 1)))
			end
		end
	end
	return nil
end

function AE_CurveTools:FindThirdPoint(moho, point1, point2)
	local points = {}
	--collect every point can be the third point (not point1, connected with point2)
	for c=0, point2:CountCurves()-1 do
		local where = -1
		local curve, where = point2:Curve(c, where)
		if where > 0 or curve.fClosed then
			local point = where > 0 and curve:Point(where-1) or curve:Point(curve:CountPoints()-1)
			if point ~= point1 then
				table.insert(points, point)
			end
		end
		if where < (curve:CountPoints()-1) or curve.fClosed then
			local point = where < (curve:CountPoints()-1) and curve:Point(where+1) or curve:Point(0)
			if point ~= point1 then
				table.insert(points, point)
			end
		end
	end
	local minAngle = math.pi * 0.1
	local point3 = nil
	local curve1, start1, dir1 = self:GetCommonSegment(moho, point2, point1)
	local curvature1 = curve1:GetCurvature(start1, moho.frame) * curve1:GetWeight(start1, moho.frame, dir1)
	local handle1 = curve1:GetControlHandle(start1, moho.frame, dir1)
	for k,v in pairs(points) do
		local curve2, start2, dir2 = self:GetCommonSegment(moho, point2, v)
		local curvature2 = curve2:GetCurvature(start2, moho.frame) * curve2:GetWeight(start2, moho.frame, dir2)
		local handle2 = curve2:GetControlHandle(start2, moho.frame, dir2)
		--get point2 incoming and outgoing handles vectors
		local v1 = point2.fPos - handle1
		local v2 = point2.fPos - handle2
		--if handles are zero or close to it get incoming and outgoing segments itself as vectors
		if curvature1 < self.curvature_accurance or curvature2 < self.curvature_accurance then	
			v1 = point2.fPos - point1.fPos
			v2 = point2.fPos - v.fPos
		end
		--find the angle between the vectors and find the closest to PI value in the total points array
		local angle1 = math.atan2(v1.x, v1.y)
		local angle2 = math.atan2(v2.x, v2.y)
		local difAngle = math.abs(angle2 - angle1)	
		if math.abs(difAngle - math.pi) < minAngle then
			minAngle = math.abs(difAngle - math.pi)
			point3 = v
		end
	end
	--return the closest to PI angle's point
	return point3
end

function AE_CurveTools:SelectPoints(moho, mouseEvent)
	local mesh = moho:DrawingMesh()
	local curveID = -1
	local segID = -1
	curveID, segID = mouseEvent.view:PickEdge(mouseEvent.pt, curveID, segID)
	if (curveID >= 0 and segID >= 0) then
		mesh:SelectNone()
		local curve = mesh:Curve(curveID)

		for ID in self:IterateCurvePoints(curve, segID + 1, -1)	do
			curve:Point(ID).fSelected = true
			if self:IsEndPoint(moho, curve, ID) then break end
		end
		
		for ID in self:IterateCurvePoints(curve, segID)	do
			curve:Point(ID).fSelected = true
			if self:IsEndPoint(moho, curve, ID) then break end
		end		
	end
end

function AE_CurveTools:SelectMulticurvePoints(moho, mouseEvent)
	local mesh = moho:DrawingMesh()
	local curveID = -1
	local segID = -1
	curveID, segID = mouseEvent.view:PickEdge(mouseEvent.pt, curveID, segID)
	if (curveID >= 0 and segID >= 0) then
		mesh:SelectNone()
		local curve = mesh:Curve(curveID)	
		local point1 = curve:Point(segID)
		local point2 = segID < (curve:CountPoints()-1) and curve:Point(segID + 1) or curve:Point(0)
		local p1 = point1
		local p2 = point2
		repeat 
			p1.fSelected = true
			p2.fSelected = true
			local p3 = self:FindThirdPoint(moho, p1, p2)
			p1 = p2
			p2 = p3
		until p2 == nil or p2.fSelected
		p2 = point1
		p1 = point2
		repeat 
			p1.fSelected = true
			p2.fSelected = true
			local p3 = self:FindThirdPoint(moho, p1, p2)
			p1 = p2
			p2 = p3
		until p2 == nil	or p2.fSelected	
	end
end

function AE_CurveTools:SelectPointGroup(moho, mouseEvent)
	local mesh = moho:DrawingMesh()
	local curveID = -1
	local segID = -1
	curveID, segID = mouseEvent.view:PickEdge(mouseEvent.pt, curveID, segID)
	if (curveID >= 0 and segID >= 0) then
		local curve = mesh:Curve(curveID)
		local p1 = curve:Point(segID)
		local p2 = (segID + 1) < curve:CountPoints() and curve:Point(segID + 1) or curve:Point(0)
		local validGroups = {}
		for g=0, mesh:CountGroups()-1 do
			local nextGroup = mesh:Group(g)
			if nextGroup:ContainsPointID(mesh:PointID(p1)) and nextGroup:ContainsPointID(mesh:PointID(p2)) then
				table.insert(validGroups, nextGroup)
			end
		end
		if #validGroups > 0 then
			local validGroup = validGroups[1 + self.clickCounter % #validGroups]
			mesh:SelectNone()
			for p = 0, validGroup:CountPoints()-1 do
				validGroup:Point(p).fSelected = true
			end
		end
	end
end

function AE_CurveTools:AddPointAtPercent(moho, targetCurve, percent, mesh, accuracy)
	if not accuracy then accuracy = 0.0000001 end
	if not mesh then mesh = moho:Mesh() end
	local newLocation = targetCurve:GetPercentLocation(percent)
	local segID = -1
	
	local returnPoint = nil

	for s=0, targetCurve:CountSegments()-1 do
		local startSeg, endSeg = targetCurve:GetSegmentRange(s, startSeg, endSeq)
		if percent >= startSeg and percent <= endSeg then
			segID = s
			if math.abs(startSeg - percent) < accuracy then
				returnPoint = targetCurve:Point(s)
				accuracy = math.abs(startSeg - percent)
			end
			if math.abs(endSeg - percent) < accuracy then
				s = s + 1
				if s >= targetCurve:CountPoints() then s = 0 end
				returnPoint = targetCurve:Point(s)		
			end	
			if returnPoint then
				return returnPoint 
			end
			mesh:AddPoint(newLocation, mesh:CurveID(targetCurve), segID, 0, true)
			local addedPointID = segID + 1
			return targetCurve:Point(addedPointID)
		end		
	end	

end

function AE_CurveTools:SetControlHandle(curve, pID, handle, frame, prePoint, syncAngles)
	local curvature = curve:GetCurvature(pID, frame)
	local oppWeight = curve:GetWeight(pID, frame, not prePoint)
	local oppLength = curvature * oppWeight
	
	curve:SetControlHandle(pID, handle, frame, prePoint, syncAngles)
	curvature = curve:GetCurvature(pID, frame)
	oppWeight = oppLength/curvature
	curve:SetWeight(pID, oppWeight, frame, not prePoint)
	
end



function AE_CurveTools:ConformSegments2Curve(moho, segmentArray, targetCurve, startPercent, endPercent, tempMesh, stickEnds)
	local mesh = moho:Mesh()
	if not tempMesh then tempMesh = mesh end

	--TODO: remove after debug
	--[[--]]
	local segs = ""
	for i,v in pairs(segmentArray) do segs = segs .. string.format("(%s %s) ", v.prePointID, v.postPointID) end
	--print(string.format ("received %s for %.2f to %.2f", segs, startPercent, endPercent))
	
	
	-- calculate total length of segment array pseudo-curve, and store each segment's current length to the same array members
	local totalLength = 0
	local smallestSegment = 1000000
	for i,v in pairs(segmentArray) do
		local segNo = math.min(v.prePointID, v.postPointID)
		if segNo == 0 and (v.prePointID ~= 1 and v.postPointID ~=1) then segNo = v.curve:CountPoints()-1 end
		v.length = v.curve:SegmentLength(segNo)
		if smallestSegment > v.length then smallestSegment = v.length end
		totalLength = totalLength + v.length
	end

	local accuracyMultiplier = math.max(#segmentArray * 2, 20)
	local accuracy = (endPercent - startPercent)/accuracyMultiplier
	local smallestSegmentPercent = (smallestSegment/totalLength)*(endPercent - startPercent)
	if accuracy > smallestSegmentPercent/2 then accuracy = smallestSegmentPercent/2 end

	-- iterate segment array calculating and setting each point's new position
	local startPoint = segmentArray[1].curve:Point(segmentArray[1].prePointID)
	local endPoint = segmentArray[#segmentArray].curve:Point(segmentArray[#segmentArray].postPointID)
	local curLength = 0
	local startPointTargetPoint = self:AddPointAtPercent(moho, targetCurve, startPercent, tempMesh, accuracy)
	for i,v in pairs(segmentArray) do
		curLength = curLength + v.length
		local pointLength = curLength/totalLength	
		local conformLength = startPercent + (endPercent - startPercent) * pointLength
		v.postPointTargetPoint = self:AddPointAtPercent(moho, targetCurve, conformLength, tempMesh, accuracy)
	end
	local endPointTargetPoint = segmentArray[#segmentArray].postPointTargetPoint
	--TODO: remove after debug
	--self:CopyCurve(targetCurve, mesh)

	--remove unused vertices from target curve, editing the rest ones to save overall form
	local segmentCounter = 0
	local segRange1 = 0
	local segRange2 = 0
	while segmentCounter <= targetCurve:CountSegments()-1 do
		segRange1, segRange2 = targetCurve:GetSegmentRange(segmentCounter, segRange1, segRange2)
		local usefulPoint = true
		if startPointTargetPoint ~= targetCurve:Point(segmentCounter) and
		segRange1 > startPercent and segRange1 <= endPercent then
			usefulPoint = false
			for i,v in pairs(segmentArray) do
				if v.postPointTargetPoint == targetCurve:Point(segmentCounter) then
					usefulPoint = true
					break
				end
			end
		end
		if usefulPoint then segmentCounter = segmentCounter + 1
		else
			--point removing manipulations
			--save handles of this and previous segment as P1, P2, Q1, Q2
			local P1 = targetCurve:GetControlHandle(segmentCounter - 1, 0, false)
			local P2 = targetCurve:GetControlHandle(segmentCounter, 0, true)
			local Q1 = targetCurve:GetControlHandle(segmentCounter, 0, false)
			local Q2 = targetCurve:GetControlHandle(segmentCounter + 1, 0, true)
			--save this, previous and next vertices as P0, Q0(P3), Q3
			local P0 = LM.Vector2:new_local()
			local Q0 = LM.Vector2:new_local()
			local Q3 = LM.Vector2:new_local()
			P0:Set(targetCurve:Point(segmentCounter - 1).fPos)
			Q0:Set(targetCurve:Point(segmentCounter).fPos)
			Q3:Set(targetCurve:Point(segmentCounter + 1).fPos)
			--calculate new t as t = (Q0 - P2)/(Q1 - P2) separately for x and y and find the average
			local tx = (Q0.x - P2.x)/(Q1.x - P2.x)
			local ty = (Q0.y - P2.y)/(Q1.y - P2.y)
			local t = (tx + ty)/2
			--calculate new R1 and R2 (outgoing and incoming handles for new single segment to replace this and previous segments)
			local R1 = (P1 - P0*(1-t))/t
			local R2 = (Q2 - Q3*t)/(1-t)
			--delete the point (using global mesh id)
			local oldPointID = tempMesh:PointID(targetCurve:Point(segmentCounter))
			tempMesh:DeletePoint(oldPointID)
			moho.layer:UpdateCurFrame()
			--and then set handles
			self:SetControlHandle(targetCurve, segmentCounter - 1, R1, 0, false, false)
			self:SetControlHandle(targetCurve, segmentCounter, R2, 0, true, false)
			moho.layer:UpdateCurFrame()
		end
		
	end
	
	--self:CopyCurve(targetCurve, mesh)
	
	--TODO: fix handles for external segments
	local globalInHandle = segmentArray[1].curve:GetControlHandle(segmentArray[1].prePointID, moho.frame, segmentArray[1].dir)
	local globalOutHandle = segmentArray[#segmentArray].curve:GetControlHandle(segmentArray[#segmentArray].postPointID, moho.frame, (not segmentArray[#segmentArray].dir))
	--apply all the calculated values to the source segment points
	if stickEnds then 
		startPointTargetPoint.fPos = startPoint.fPos
		endPointTargetPoint.fPos = endPoint.fPos
	end
	if stickEnds then 
		startPoint.fPos:Set(startPointTargetPoint.fPos)
	end
	for i,v in pairs(segmentArray) do		
		if i < #segmentArray or not stickEnds then
			local thePoint = v.curve:Point(v.postPointID)
			thePoint.fPos:Set(v.postPointTargetPoint.fPos)
		end
	end
	moho:AddPointKeyframe(moho.drawingFrame)

	local delta = startPointTargetPoint.fPos - startPoint.fPos
	startPoint.fPos:Set(startPointTargetPoint.fPos + delta)
	for i,v in pairs(segmentArray) do		
		local thePoint = v.curve:Point(v.postPointID)
		delta = v.postPointTargetPoint.fPos - thePoint.fPos
		thePoint.fPos:Set(v.postPointTargetPoint.fPos + delta)
	end
	moho:AddPointKeyframe(moho.drawingFrame)	
	
	
	moho.layer:UpdateCurFrame()	
	self:SetControlHandle(segmentArray[1].curve, segmentArray[1].prePointID, globalInHandle, moho.frame, segmentArray[1].dir, false)
	self:SetControlHandle(segmentArray[#segmentArray].curve, segmentArray[#segmentArray].postPointID, globalOutHandle, moho.frame, (not segmentArray[#segmentArray].dir), false)
	
	--apply values to handles
	local targetCurvePointID = targetCurve:PointID(startPointTargetPoint)
	local theHandle = targetCurve:GetControlHandle(targetCurvePointID, 0, false)
	self:SetControlHandle(segmentArray[1].curve, segmentArray[1].prePointID, theHandle, moho.frame, not segmentArray[1].dir, false)		
	for i,v in pairs(segmentArray) do
		targetCurvePointID = targetCurve:PointID(v.postPointTargetPoint)
		theHandle = targetCurve:GetControlHandle(targetCurvePointID, 0, true)	
		self:SetControlHandle(v.curve, v.postPointID, theHandle, moho.frame, v.dir, false)
		if i < #segmentArray then
			w = segmentArray[i+1]
			theHandle = targetCurve:GetControlHandle(targetCurvePointID, 0, false)
			self:SetControlHandle(w.curve, w.prePointID, theHandle, moho.frame, not w.dir, false)
		end		
	end
	
	moho.layer:UpdateCurFrame()
end

function AE_CurveTools:PrintPointInfo(curve, pID, frame)
	local inOffset = curve:GetOffset(pID, frame, true)
	local outOffset = curve:GetOffset(pID, frame, false)
	local inHandle = curve:GetControlHandle(pID, frame, true)
	local outHandle = curve:GetControlHandle(pID, frame, false)
	print(string.format("point %s offset %.2f %.2f handles {%.2f, %.2f} {%.2f, %.2f}", pID, inOffset, outOffset, inHandle.x, inHandle.y, outHandle.x, outHandle.y))
end

function AE_CurveTools:CreateCircleCurve(moho, p1, p2, p3, p4, mesh)

	mesh:AddLonePoint(p4, 0)
	mesh:AppendPoint(p1, 0)
	mesh:AppendPoint(p2, 0)
	mesh:AppendPoint(p3, 0)
	mesh:AppendPoint(p4, 0)
	mesh:WeldPoints(mesh:CountPoints()-5, mesh:CountPoints()-1,0)
	local circleCurve = mesh:Curve(mesh:CountCurves()-1)
	for i=0,3 do
		circleCurve:SetCurvature(i, 0.3914, 0)
		circleCurve:SetWeight(i,1,0,false)
		circleCurve:SetWeight(i,1,0,true)
		circleCurve:SetOffset(i, 0, 0, false)
		circleCurve:SetOffset(i, 0, 0, true)	
	end
	return circleCurve
end

function AE_CurveTools:CirculefySelectedPoints(moho)
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)

	-- collect currently selected points to array of segments following by each other
	local mesh = moho:Mesh()
	if not mesh then return LM.GUI.Alert(LM.GUI.ALERT_WARNING, "Not found mesh") end
	--local segmentArray = self:CollectVertexLineFromSelection(mesh)
	if not self.segmentArray or #self.segmentArray < 1 then return end
	local segmentArray = self.segmentArray
	
	local startPoint = segmentArray[1].curve:Point(segmentArray[1].prePointID)
	local endPoint = segmentArray[#segmentArray].curve:Point(segmentArray[#segmentArray].postPointID)

	--calculate new circle parameters
	local distance = (startPoint.fPos - endPoint.fPos):Mag()
	local parameter = math.abs(self.circulefy_parameter)
	local radius = distance *(1 + parameter*parameter)/(4*parameter)
	local lineCenter = startPoint.fPos + (endPoint.fPos - startPoint.fPos)/2
	local toCircleCenter = math.sqrt(radius*radius - distance*distance/4)
	local circleCenterDirection = (endPoint.fPos - startPoint.fPos):Norm()*(self.circulefy_parameter > 0 and 1 or -1)
	circleCenterDirection:Set(circleCenterDirection.y, -circleCenterDirection.x)
	local circleCenter = lineCenter + (circleCenterDirection * toCircleCenter)
	local circlePoint1Pos = circleCenter + (startPoint.fPos - lineCenter):Norm()*radius
	local circlePoint3Pos = circleCenter + (endPoint.fPos - lineCenter):Norm()*radius
	local directedRadius = circleCenterDirection * radius
	local circlePoint2Pos = circleCenter - directedRadius 
	local circlePoint4Pos = circleCenter + directedRadius

	
	--create the temporal circle
	local mesh_preview = MOHO.MeshPreview(200, 200)
	local tempMesh = mesh_preview:Mesh()
	local circleCurve, mesh, mesh_preview = self:CreateCircleCurve(moho, circlePoint1Pos, 
		circlePoint2Pos, circlePoint3Pos, circlePoint4Pos, tempMesh)

	
	--get startPercent and endPercent for a circle, by calculating the angle and dividing it by PI
	local curvePart = math.asin(distance/(2*radius))/math.pi
	local startPercent = 0.25 - (curvePart/2)
	local endPercent = startPercent + curvePart
	--[[--]]	
	--conform segment array to the part of circle curve
	self:ConformSegments2Curve(moho, segmentArray, circleCurve, startPercent, endPercent, tempMesh, true)

	
end

function AE_CurveTools:CreateFreehandCurve(moho)

	if #self.sketchPts <= 0 then return end
	
	local mesh_preview = MOHO.MeshPreview(200, 200)
	local tempMesh = mesh_preview:Mesh()
	local mesh = tempMesh
	
	mesh:AddLonePoint(self.sketchPts[1], 0)
	for i=2, #self.sketchPts do
		mesh:AppendPoint(self.sketchPts[i], 0)
	end
	local createdCurve = mesh:Curve(mesh:CountCurves()-1)
	
	
	-- Look for sharp angles that should be converted into corner points
	local cornerThreshold = math.cos(math.rad(70)) -- adjust this angle to make the script more or less sensitive to corners
	local curve = createdCurve
	for i = 0, curve:CountPoints() - 1 do
		if (curve.fClosed or (i > 0 and i < curve:CountPoints() - 1)) then
			local v1 = curve:Point(i + 1).fPos - curve:Point(i).fPos
			local v2 = curve:Point(i).fPos - curve:Point(i - 1).fPos
			v1:NormMe()
			v2:NormMe()
			if (v1:Dot(v2) < cornerThreshold) then
				local curvature = curve:Curvature(i)
				curvature:SetValue(moho.drawingLayerFrame, MOHO.PEAKED)
				if (moho.drawingLayerFrame ~= 0) then
					curvature:SetValue(0, MOHO.PEAKED)
					if (curvature:CountKeys() > 1) then
						curvature:DeleteKey(moho.drawingLayerFrame)
					end
				end
			end
		end
	end
	
	local epsilon = MOHO.SimplifyEpsilon(LM_Freehand.simplifyValue)
	createdCurve:SelectCurvePoints()
	mesh:AdvancedCurveSimplification(mesh:CurveID(createdCurve), 0, 0.0, epsilon, 0.01)
	
	--check self.sketchPtsTiming for great differences in points timing
	local diffs = {}
	for i=2, #self.sketchPtsTiming do table.insert(diffs, self.sketchPtsTiming[i]-self.sketchPtsTiming[i-1]) end
	local cornerPoses = {}
	for i=1, #diffs-1 do if diffs[i]/diffs[i+1] > 5 then table.insert(cornerPoses, self.sketchPts[i]) end end
	local cornerPoints = {}
	for i, pos in pairs(cornerPoses) do
		local closestMag = 1000
		local foundPointID = -1
		for j = 1, curve:CountPoints() - 2 do
			local nextMag = (curve:Point(j).fPos - pos):Mag()
			if  nextMag < closestMag then
				closestMag = nextMag
				foundPointID = j
			end
		end
		table.insert(cornerPoints, foundPointID)
	end

	self:PrepareAndRunConform(moho, createdCurve, tempMesh)

end

function AE_CurveTools:CreateSelectedCopyCurve(moho)
	--TODO: for past-tuning move this Mesh or MeshPreview or both into object properties
	local mesh_preview = MOHO.MeshPreview(200, 200)
	local tempMesh = mesh_preview:Mesh()

	print(tostring(self.targetLayerCurve))
	local targetCurve = self:CopyCurve(self.targetLayerCurve, tempMesh)
	print(tostring(targetCurve))
	self:PrepareAndRunConform(moho, targetCurve, tempMesh)
	
end

function AE_CurveTools:PrepareAndRunConform(moho, targetCurve, tempMesh)

	if self.segmentArray and #self.segmentArray > 0 then
	
		moho.document:SetDirty()
		moho.document:PrepUndo(nil)
		
		if self.conformModeMenu:FirstCheckedMsg() == self.CONFORM_LENGTH then		
			self:ConformSegments2Curve(moho, self.segmentArray, targetCurve, 0, 1, tempMesh)
		else 
			local segmentArrays, targetCurveParts = self:SplitCurves(moho, self.segmentArray, targetCurve, cornerPoints)
			for i=1, #segmentArrays do
				self:ConformSegments2Curve(moho, segmentArrays[i], targetCurve, targetCurveParts[i][1].start, targetCurveParts[i][#targetCurveParts[i]].finish, tempMesh)
			end			
		end
		
		moho.layer:UpdateCurFrame()
		moho:UpdateUI()
		moho.view:DrawMe()
		
		--TODO: remove after debug
		--self:CopyCurve(targetCurve, moho:Mesh())
	end
	
	
	
end

function AE_CurveTools:SplitCurves(moho, segmentArray, targetCurve, pausePoints)
	local segmentArrays = {}
	local targetCurveParts = {}
	
	--find and collect corner vertices for target curve
	local curveSegmentNumbers = {}
	if not pausePoints or #pausePoints == 0 then
		for p = 1, targetCurve:CountPoints()-1 do
			local dif = math.abs(targetCurve:GetOffset(p, moho.frame, true) - targetCurve:GetOffset(p, moho.frame, false))
			if dif > 0.001 or p == targetCurve:CountPoints()-1 then
				local startSegment = 0
				if #curveSegmentNumbers > 0 then
					local lastGroup = curveSegmentNumbers[#curveSegmentNumbers]
					startSegment = lastGroup[#lastGroup] + 1
				end
				local nextGroup = {}
				for j = startSegment, p-1 do table.insert(nextGroup, j) end
				table.insert(curveSegmentNumbers, nextGroup)
			end
		end
	else
		table.insert(pausePoints, targetCurve:CountPoints()-1)
		for i, v in pairs(pausePoints) do
			local nextGroup = {}
			local startPoint = 0
			if i > 1 then startPoint = pausePoints[i-1] end
			for j = startPoint, v-1 do table.insert(nextGroup, j) end
			table.insert(curveSegmentNumbers, nextGroup)				
		end	
	end
	local totalCurveGroups = {}
	for i, numberGroup in pairs(curveSegmentNumbers) do
		local copyGroup = {}
		for j, curveSegment in pairs(numberGroup) do
			local range1, range2 = targetCurve:GetSegmentRange(curveSegment)
			local postPoint = curveSegment + 1
			local segmentItem = {["start"] = range1, ["finish"] = range2}
			table.insert(copyGroup, segmentItem)
		end
		table.insert(totalCurveGroups, copyGroup)
	end
	
	--run through segmentArray checking corner points like FindThirdPoint (extract check method from it)
	local totalLength = 0
	for i,v in pairs(segmentArray) do
		local segNo = math.min(v.prePointID, v.postPointID)
		if segNo == 0 and (v.prePointID ~= 1 and v.postPointID ~=1) then segNo = v.curve:CountPoints()-1 end
		v.length = v.curve:SegmentLength(segNo)
		totalLength = totalLength + v.length
	end	
	
	local segmentNumbers = {}
	local cumulativeLength = 0
	local minAngle = math.pi * 0.1
	for i, v in pairs(segmentArray) do
		cumulativeLength = cumulativeLength + v.length
		v.finish = cumulativeLength/totalLength
		local dif = 0
		if i < #segmentArray then
			local vv = segmentArray[i + 1] 
			local curvature1 = v.curve:GetCurvature(v.postPointID, moho.frame) * v.curve:GetWeight(v.postPointID, moho.frame, v.dir)
			local curvature2 = vv.curve:GetCurvature(vv.prePointID, moho.frame) * vv.curve:GetWeight(vv.prePointID, moho.frame, (not vv.dir))			

			local v1 = v.curve:GetControlHandle(v.postPointID, moho.frame, v.dir) - v.curve:Point(v.postPointID).fPos
			local v2 = vv.curve:GetControlHandle(vv.prePointID, moho.frame, (not vv.dir)) - vv.curve:Point(vv.prePointID).fPos
			if curvature1 < self.curvature_accurance or curvature2 < self.curvature_accurance then 
				v1 = v.curve:Point(v.prePointID).fPos - v.curve:Point(v.postPointID).fPos			
				v2 = vv.curve:Point(vv.postPointID).fPos - vv.curve:Point(vv.prePointID).fPos
			end
			local a1 = math.atan2(v1.x, v1.y)
			local a2 = math.atan2(v2.x, v2.y)
			dif = math.abs(math.pi - math.abs(a1 - a2))
		end
		if dif > minAngle or i == #segmentArray then			
			local startNumber = 1
			if #segmentNumbers > 0 then 
				local lastGroup = segmentNumbers[#segmentNumbers]
				startNumber = lastGroup[#lastGroup] + 1
			end
			local nextGroup = {}
			for j = startNumber, i do table.insert(nextGroup, j) end
			table.insert(segmentNumbers, nextGroup)			
		end	
	end
	local totalSegmentGroups = {}
	for i, numberGroup  in pairs(segmentNumbers) do
		local copyGroup = {}
		for j, segmentNumber in pairs(numberGroup) do
			table.insert(copyGroup, segmentArray[segmentNumber])
		end
		table.insert(totalSegmentGroups, copyGroup)
	end
	local complicatedSegments, complicatedCurveParts = self:SplitCurve(moho, totalSegmentGroups, totalCurveGroups)
	--remove sublevels from complicated arrays
	--turning it into one-level array of segmentArrays and curvParts respectively
	self:UnpackArray(complicatedSegments, segmentArrays, true)
	self:UnpackArray(complicatedCurveParts, targetCurveParts, false)
	
	return segmentArrays, targetCurveParts

end

function AE_CurveTools:UnpackArray(sourceArray, targetArray)
	while #sourceArray > 0 do
		local nextItem = table.remove(sourceArray, 1)
		if nextItem[1].finish ~= nil then 
			table.insert(targetArray, nextItem)
		else			
			self:UnpackArray(nextItem, targetArray, sublevel)
		end
	end
end

function AE_CurveTools:SplitCurve(moho, segmentArrays, curveParts, allwaysSplit1, allwaySplit2)
	--TODO: remove after debug
	print("split received ", #segmentArrays, " and ", #curveParts)
	--if two items in both arrays do nothing
	if #segmentArrays == 1 and #curveParts == 1 then return segmentArrays, curveParts end	
	if #segmentArrays > 1 and #curveParts == 1 then 
		local newGroup = {}		
		if #curveParts[1] > 1 and not allwaySplit1 then
			--if you can, split curveParts with vertices
			for i, v in pairs(curveParts[1]) do table.insert(newGroup, {v}) end
			allwaySplit2 = true
		elseif allwaySplit1 then
			--stick back segments
			for i, v in pairs(segmentArrays) do
				for j, w in pairs(v) do table.insert(newGroup, w) end
			end
			return newGroup, curveParts
		else 
			--otherwise split it with percents
			local commonLength = 0	
			local groupLengths = {}
			for i, v in pairs(segmentArrays) do 
				local groupLength = 0
				for j, w in pairs(v) do groupLength = groupLength + w.length end
				commonLength = commonLength + groupLength
				table.insert(groupLengths, groupLength)
			end		
			for i, length in pairs(groupLengths) do 
				local start = curveParts[1][1].start
				if #newGroup > 1 then start = newGroup[#newGroup][1].finish end
				local finish = (curveParts[1][1].finish - curveParts[1][1].start) * length/commonLength + start
				table.insert(newGroup, {{["start"] = start, ["finish"] = finish}})
			end
		end
		curveParts = newGroup
	end 
	if #segmentArrays == 1 and #curveParts > 1 then 
		local newGroup = {}			
		if #segmentArrays[1] > 1 and not allwaySplit2 then 
			--if you can, split segmentArrays by vertices
			for i, v in pairs(segmentArrays[1]) do table.insert(newGroup, {v}) end
			allwaysSplit1 = true
		else
			--otherwise stick curveParts
			for i, v in pairs(curveParts) do 
				for j, w in pairs(v) do table.insert(newGroup, w) end
			end
			return segmentArrays, newGroup
		end
		segmentArrays = newGroup
	end	
	--if three items in both arrays -- divide them both by second vertex
	if #segmentArrays == 2 and #curveParts == 2 then return segmentArrays, curveParts end	
	--if more then three items in any array find two closest vertices
	local minDistance = 1
	local splitI = -1
	local splitJ = -1
	for i = 1, #segmentArrays - 1 do
		for j = 1, #curveParts - 1 do
			--get last percent of each group and find percent distance between them
			local lastSegment = segmentArrays[i][#segmentArrays[i]]
			local lastCurvePart = curveParts[j][#curveParts[j]]
			local dist = math.abs(lastSegment.finish - lastCurvePart.finish)
			if dist < minDistance then
				minDistance = dist
				splitI = i
				splitJ = j
			end
		end
	end
	
	--divide by closest vertices and recurse call this method for both of divided pairs
	local leftSegmentArrays = {}
	for i = 1, splitI do
		table.insert(leftSegmentArrays, table.remove(segmentArrays, 1))
	end
	local leftCurveParts = {}
	for j = 1, splitJ do
		table.insert(leftCurveParts, table.remove(curveParts, 1))
	end
	if allwaysSplit1 then
		--combine groups inside left and right segmentArrays 
		local newGroup = {}
		for i = 1, splitI do table.insert(newGroup, table.remove(leftSegmentArrays, 1)[1]) end
		leftSegmentArrays = {newGroup}
		newGroup = {}
		local n = #segmentArrays 
		for i = 1, n do table.insert(newGroup, table.remove(segmentArrays, 1)[1]) end
		segmentArrays = {newGroup}
		allwaysSplit1 = false
	end
	if allwaysSplit2 then
		--combine groups inside left and right curveParts 
		local newGroup = {}
		for i = 1, splitJ do table.insert(newGroup, table.remove(leftCurveParts, 1)[1]) end
		leftCurveParts = {newGroup}
		newGroup = {}
		local n = #segmentArrays 
		for i = 1, n do table.insert(newGroup, table.remove(curveParts, 1)[1]) end
		curveParts = {newGroup}
		allwaysSplit2 = false		
	end	

	leftSegmentArrays, leftCurveParts = self:SplitCurve(moho, leftSegmentArrays, leftCurveParts, allwaysSplit1, allwaySplit2)
	segmentArrays, curveParts = self:SplitCurve(moho, segmentArrays, curveParts, allwaysSplit1, allwaySplit2)
	
	return {leftSegmentArrays, segmentArrays}, {leftCurveParts, curveParts}
end

function AE_CurveTools:CopyCurve(sourceCurve, targetMesh)
	targetMesh:AddLonePoint(sourceCurve:Point(0).fPos, 0)
	for i = 1, sourceCurve:CountPoints()-1 do
		targetMesh:AppendPoint(sourceCurve:Point(i).fPos, 0)
	end	
	local targetCurve = targetMesh:Curve(targetMesh:CountCurves()-1)
	
	for p=0, sourceCurve:CountPoints()-1 do
		--get handles for old curve point
		local inHandle = sourceCurve:GetControlHandle(p, 0, true)
		local outHandle = sourceCurve:GetControlHandle(p, 0, false)
		--set handles for new curve point
		targetCurve:SetControlHandle(p, inHandle, 0, true, false)
		targetCurve:SetControlHandle(p, outHandle, 0, false, false)
		--get incoming magnitude for old curve point
		local oldMag = (inHandle - sourceCurve:Point(p).fPos):Mag()
		local newInHandle = targetCurve:GetControlHandle(p, 0, true)
		local newMag = (newInHandle - targetCurve:Point(p).fPos):Mag()
		if math.abs(newMag - oldMag) > 0.00001 then
			local multiplier = oldMag/newMag
			local currentWeight = targetCurve:GetWeight(p, 0, true)
			targetCurve:SetWeight(p, currentWeight * multiplier, 0, true)
		end
	end
	return targetCurve
end

function AE_CurveTools:InvertSegmentArray(moho, segmentArray)
	local flipedSegmentArray = {}
	for i = #segmentArray, 1, -1 do 
		local src = segmentArray[i]
		local nextItem = {["prePointID"] = src.postPointID, ["postPointID"] = src.prePointID, ["dir"] = not src.dir, ["curve"] = src.curve }
		table.insert(flipedSegmentArray, nextItem)
	end
	return flipedSegmentArray
end

function AE_CurveTools:SafePointRemove(moho, mesh, curve, curvePointID)
	--point removing manipulations
	--save handles of this and previous segment as P1, P2, Q1, Q2
	local P1 = curve:GetControlHandle(curvePointID - 1, 0, false)
	local P2 = curve:GetControlHandle(curvePointID, 0, true)
	local Q1 = curve:GetControlHandle(curvePointID, 0, false)
	local Q2 = curve:GetControlHandle(curvePointID + 1, 0, true)
	--save this, previous and next vertices as P0, Q0(P3), Q3
	local P0 = LM.Vector2:new_local()
	local Q0 = LM.Vector2:new_local()
	local Q3 = LM.Vector2:new_local()
	P0:Set(curve:Point(curvePointID - 1).fPos)
	Q0:Set(curve:Point(curvePointID).fPos)
	Q3:Set(curve:Point(curvePointID + 1).fPos)
	
	--calculate t 
	--old formula --local t = ((Q0 - P2):Mag()/(Q1 - P2):Mag())
	--get t from curve length instead
	local range1, range2 = curve:GetSegmentRange(curvePointID - 1) 
	local range3 = curve:GetSegmentRange(curvePointID + 1)
	local t = (range2 - range1)/(range3 - range1)
	--calculate new R1 and R2 (outgoing and incoming handles for new single segment to replace this and previous segments)
	local R1 = (P1 - P0*(1-t))/t
	local R2 = (Q2 - Q3*t)/(1-t)
	
	--TODO: some claritications for R1 and R2
	--with self:ClarifyNewHandles(P0, R1, R2, Q3, t, Q0, 2, 0.001)
	
	
	--delete the point (using global mesh id)
	local oldPointID = mesh:PointID(curve:Point(curvePointID))
	mesh:DeletePoint(oldPointID)
	moho.layer:UpdateCurFrame()
	--and then set handles
	self:SetControlHandle(curve, curvePointID - 1, R1, 0, false, false)
	self:SetControlHandle(curve, curvePointID, R2, 0, true, false)
	moho.layer:UpdateCurFrame()
end

function AE_CurveTools:GetBezierPercent(P0, P1, P2, P3, t) 	
	return P0*((1-t)^3) + P1*(3*((1-t)^2)*t) + P2*(3*(1-t)*(t^2)) + P3 * (t^3)
end

function AE_CurveTools:ClarifyNewHandles(P0, P1, P2, P3, t, targetQ, handleFactor, accuracy)
	--TODO: recursively change handleFactor to reach closest point to targetQ. 
	--TODO: stop recursion when distance is smaller then accuracy or when distance starts to grow
	--TODO: for formulas see my notes in zim
end


-- **************************************************
-- Localization
-- **************************************************

function AE_CurveTools:Localize(text)
	local phrase = {}

	phrase['Description'] = 'Various operations with vertices, curves, etc. W.I.P.'
	phrase['UILabel'] = 'Curve Tools'

	phrase['L to R'] = 'L to R'
	phrase['flip'] = 'flip'

	local fileWord = MOHO.Localize("/Menus/File/File=File")
	if fileWord == "Файл" then
		phrase['Description'] = 'Various operations with vertices, curves, etc. W.I.P.'
		phrase['UILabel'] = 'Curve Tools'

		phrase['L to R'] = 'L to R'
		phrase['flip'] = 'flip'
	end

	return phrase[text]
end


