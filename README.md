# Instalation: 

Download zip and use it for **Scripts -> Install script..."** Moho menu command **or**:

- Copy .lua and (optional) .png files from **tool** to your **[moho content folder]/scripts/tool** folder.
- Copy resources from **ScriptResources/curvetools** to **[moho content folder]/scripts/ScriptResources/curvetools** folder.

For sinchronising with git your can clone root directory and make symbolic links from **tool** and **ScriptResources/curvetools** subfolders to **[moho content folder]/scripts/tool/curvetools** and **[moho content folder]/scripts/ScriptResources/curvetools**

Having subfolder in **[moho content folder]/scripts/tool** You have to list the tool in Your **_tool_list.txt** with subfolder in path:

`tool    curvetools/ae_curve_tools   ...`

# TODO:
- for Flip command:
  - [ ] process any curves having common points with starting curve (as an option which can be turned on and off)
  - [ ] add an option for fliping all points around x=0 or around local center
- for Straighten command:
  - [ ] also, process any curves connected to selected points
- other commands
  - place selected vertices along other useful forms, such as:
    - [ ] circle
	- [ ] arc
	- [ ] some freehand drawn form
  - special tools for selecting curve segments and fast storing new selections